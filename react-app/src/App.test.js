import React from 'react';
import { render, screen, fireEvent } from '@testing-library/react';
import CalculatorApp from './App/App';

test('Simple test : initial value to be 0', () => {
  
  render(<CalculatorApp />);

  expect( screen.getByTestId("h1").innerText ).toBe(undefined);
  
});